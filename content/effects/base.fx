struct OutputVS {
    float4 posH   : POSITION0;
	float2 Tex   : TEXCOORD0;
};

OutputVS TransformedVS(float4 posL : POSITION0,float2 Tex: TEXCOORD0) {
	OutputVS outVS = (OutputVS)0;	
	outVS.posH = posL;	
	outVS.Tex = Tex; 
    return outVS;
}
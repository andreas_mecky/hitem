#include "base.fx"

uniform extern texture gTex;
float timer = 0.0f;
float3 grayscale = { 0.2125f, 0.7154f, 0.0721f };
float total = 4.0f;

float2 samples[12]
=	{
		-0.326212, -0.405805,
		-0.840144, -0.073580,
		-0.695914,  0.457137,
		-0.203345,  0.620716,
		 0.962340, -0.194983,
		 0.473434, -0.480026,
		 0.519456,  0.767022,
		 0.185461, -0.893124,
		 0.507431,  0.064425,
		 0.896420,  0.412458,
		-0.321940, -0.932615,
		-0.791559, -0.597705
	};

sampler TexS = sampler_state {
	Texture = <gTex>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	//AddressU  = WRAP;
    //AddressV  = WRAP;
};

float4 BlurPS(float2 Tex : TEXCOORD0) : COLOR {    
	float4 color = tex2D( TexS, Tex.xy);
	float BlurFactor = 0.005; //set between 0.001 and 0.05 

	for (int i = 0; i < 12; i++) {
		color += tex2D(TexS, Tex + BlurFactor * samples[i]);
	}

	return color / 13;
}

technique BlurTech {
    pass P0 {
        vertexShader = compile vs_2_0 TransformedVS();
        pixelShader  = compile ps_2_0 BlurPS();
    }
}

#pragma once
#include <world\World.h>
#include <world\SpriteEntity.h>

const int MAX_ITEMS = 20;

class Goal : public ds::GameObject {

struct Item {
	ds::SpriteEntity entity;
	float timer;
	float angle;
	int dir;
};

public:
	Goal(void);
	~Goal(void);
	void update(float elapsed);
	void init();
private:
	Item m_Items[MAX_ITEMS*4];
};


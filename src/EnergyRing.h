#pragma once
#include <nodes\SpriteBatch.h>

class EnergyRing {

public:
	EnergyRing(ds::SpriteBatch* spriteBatch,const ds::Vec2& center);
	~EnergyRing(void);
	void draw(int percentage);
private:
	ds::SpriteBatch* m_SpriteBatch;
	ds::Vec2 m_Center;	
};


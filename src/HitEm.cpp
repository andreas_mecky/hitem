#include "HitEm.h"
#include "utils\Log.h"
#include "Constants.h"

ds::BaseApp *app = new HitEm(); 
//const char* BALL_TEXTURES[] = {"Red_Ball","Yellow_Ball","Green_Ball","Blue_Ball"};

HitEm::HitEm() : ds::BaseApp() {
	m_Width = 1024;
	m_Height = 768;
	m_ClearColor = ds::Color(10,10,10,255);	
	//m_ClearColor = ds::Color(0,0,0,255);	
	m_StickyBall = 0;
}

HitEm::~HitEm() {
	//delete m_QuadNode;
	//delete m_SimpleQuadNode;
}

// -------------------------------------------------------
// Load content and prepare game
// -------------------------------------------------------
bool HitEm::loadContent() {	

	//m_DefaultBS = renderer->createBlendState(ds::BL_SRC_ALPHA,ds::BL_ONE_MINUS_SRC_ALPHA,true);
	m_AddBlendState = renderer->createBlendState(ds::BL_ONE,ds::BL_ONE,true);

	//int texID = renderer->loadTexture("textures");
	//assert( texID != -1 );
	//m_Mtrl = renderer->createMaterial("BaseMaterial",texID);
	//int atlas = renderer->loadTextureAtlas("MyAtlas");
	//renderer->setTextureAtlas(m_Mtrl,atlas);	

	int id = m_World.createSpriteBatch("textures");	



	//m_World.loadSettings("game",&m_GameSettings);

	m_World.addSpriteEntity(0,0,&m_Background,512,384,ds::Rect(300,400,400,400),0.0f,2.0f,2.0f);	
	m_World.addSpriteEntity(0,0,&m_Catcher,512,384,ds::Rect(30,220,80,80));
	m_Catcher.setActive(false);
	m_World.addSpriteEntity(0,0,&m_Bat,512,384,ds::Rect(32,0,45,41));
	m_World.createGameObject<Goal>(&m_Goal);
	m_World.addParticleSystemEntity(2,id,"border_exp",&m_BorderExp,600,m_AddBlendState);
	m_BorderEmitter = m_BorderExp.getEmitter<ds::ConeEmitter>();
	/*
	ds::BitmapFont bf;
	bf.startChar = 32;
	bf.endChar = 128;
	bf.charHeight = 14;
	bf.gridHeight = 20;
	bf.startX = 0;
	bf.startY = 125;
	bf.width =	405;
	bf.height = 168;
	bf.padding = 6;
	bf.textureSize = 1024;
	*/
	//renderer->cre(bf,texID,ds::Color(1.0f,0.0f,1.0f,1.0f));

	// balls
	createBall(0);
	//createBall(1);
	//createBall(2);
	//createBall(3);

	//m_RedRing = new EnergyRing(m_SpriteBatch,ds::Vec2(800,600));
	//m_GreenRing = new EnergyRing(m_SpriteBatch,ds::Vec2(800,1500));
	/*
	// particles
	m_BounceParticles = new BounceParticlesystem(renderer,mtrl);

	// render target
	ds::Shader* fadeShader = renderer->loadShader("blur","BlurTech");	
	assert(fadeShader!=0);
	m_PostProcessor = new ds::PostProcessor(renderer);
	fadeShader->setTexture("gTex",m_PostProcessor->getTexture());
	fadeShader->setFloat("timer",4.0f);
	m_PostProcessor->setShader(fadeShader);
	*/
	return true;
}

// -------------------------------------------------------
// Update
// -------------------------------------------------------
void HitEm::update(const ds::GameTime& gameTime) {
	ds::Vec2 mp = getMousePos();
	//m_Bat.position = mp;
	float angle = 0.0f;
	mp.y = 768.0f - mp.y;
	/*
	ds::Vec2 mp = getMousePos();
	//m_Bat.position = mp;
	float angle = 0.0f;
	m_HUD->update(gameTime.elapsed);
	m_BounceParticles->update(gameTime.elapsed);
	*/
	ds::Vec2 bp = m_Bat.getPosition();
	ds::math::follow(mp,bp,&angle,2.0f,5.0f*gameTime.elapsed);
	m_Bat.setPosition(bp);
	m_Catcher.setPosition(bp);
	/*
	if ( !isOutside(mp) ) {
		m_ObjectsQuadNode->update(m_Bat.index,m_Bat.position);
		m_OuterRingNode->update(m_OuterRingIdx,m_Bat.position);
	}
	*/

	
	for ( int i = 0; i < 4; ++i ) {
		Ball* b = &m_Balls[i];
		if ( !b->sticky ) {
			if ( b->mode == BM_GROWING ) {
				b->timer += gameTime.elapsed;
				float norm = b->timer / BALL_GROW_TTL;
				float scale = 0.1f + norm * 0.9f;
				b->entity.setScale(scale,scale);				
				if ( b->timer >= BALL_GROW_TTL ) {
					b->mode = BM_FLYING;
					b->entity.setScale(1.0f,1.0f);				
				}
			}
			else {
				ds::Vec2 bp = b->entity.getPosition();
				ds::vector::addScaled(bp,b->velocity,gameTime.elapsed);	
				b->entity.setPosition(bp);
				float mx = bp.x - CENTER_X;
				float my = bp.y - CENTER_Y;
				float r = sqrt(mx * mx + my*my);
				if ( r > ( RING_RADIUS - HALF_SIZE ) ) {
					float da = ds::math::getAngle(ds::vector::normalize(b->velocity),ds::Vec2(1,0));
					LOG(logINFO) << "da " << RADTODEG(da);
					int goal = checkGoal(bp.x,bp.y);
					if ( goal != -1 ) {
						LOG(logINFO) << "Goal " << goal << " ball " << b->colorIndex;
						/*
						if ( goal == b->colorIndex ) {							
							m_HUD->addPercentage(goal,5);
						}
						else {
							m_HUD->addPercentage(goal,-2);
						}
						*/
						respawn(b);
					}
					else {
						//LOG(logINFO) << "ball " << b->index << " bouncing at " << b->position.x << " " << b->position.y;
						//m_BounceParticles->start(b->position,b->angle);
						b->angle += 180.0f;
						if ( b->angle > 360.0f ) {
							b->angle -= 360.0f;
						}
						b->velocity.x *= -1.0f;
						b->velocity.y *= -1.0f;
						ds::vector::addScaled(bp,b->velocity,gameTime.elapsed);
						b->entity.setPosition(bp);
						float cvs = ds::math::getAngle(b->velocity,ds::Vec2(1,0));
						ds::Vec2 expPos = bp;
						ds::vector::addRadial(expPos,-30.0f,cvs);
						float va = RADTODEG(ds::math::reflect(cvs));
						if ( va > 360.0f ) {
							va -= 360.0f;
						}
						LOG(logINFO) << "VA " << va;
						m_BorderEmitter->setAngle(va - 45.0f,va + 45.0f);
						m_BorderExp.start(expPos);
					}
				}
			}
			ds::Vec2 diff = b->entity.getPosition() - m_Bat.getPosition();
			if ( ds::vector::sqrLength(diff) <= (( BALL_RADIUS+PLAYER_RADIUS) * (BALL_RADIUS+PLAYER_RADIUS)) ) {
				ds::Vec2 norm = ds::vector::normalize(diff);
				ds::Vec2 pushBack = ds::math::getShiftVector(b->entity.getPosition(),BALL_RADIUS,m_Bat.getPosition(),PLAYER_RADIUS+2.0f);
				pushBack *= 1.1f;
				b->entity.setPosition(b->entity.getPosition() + pushBack);
				b->velocity = reflect(b->velocity,norm);
			}
		}
		// sticky ball
		else {
			b->entity.setPosition(m_Bat.getPosition());
			ds::Vec2 bp = b->entity.getPosition();
			b->angle += DEGTORAD(270.0f) * gameTime.elapsed;
			ds::vector::addRadial(bp,40.0f,b->angle);
			b->entity.setPosition(bp);
		}
	}
	
	/*
	for ( int i = 0; i < 4; ++i ) {
		m_Goals[i]->update(gameTime.elapsed);
	}
	*/
}

// -------------------------------------------------------
// Dot product of two vectors
// -------------------------------------------------------
float HitEm::dot(const ds::Vec2& v1, const ds::Vec2& v2) {
	return v1.x * v2.x + v1.y * v2.y;
}
// -------------------------------------------------------
// Reflect
// -------------------------------------------------------
ds::Vec2 HitEm::reflect(const ds::Vec2& vel,const ds::Vec2& norm) {
	float dp = ds::vector::dot(vel,norm);
	return ds::Vec2(vel.x - 2.0f * dp * norm.x,vel.y -2.0f * dp * norm.y);	
}

// -------------------------------------------------------
// isOutside - checks if position is outside of ring
// -------------------------------------------------------
bool HitEm::isOutside(const ds::Vec2& pos) {
	float mx = pos.x - CENTER_X;
	float my = pos.y - CENTER_Y;
	float r = sqrt(mx * mx + my*my);
	if ( r > ( RING_RADIUS - BALL_SIZE ) ) {
		return true;
	}
	return false;
}

// -------------------------------------------------------
// Draw
// -------------------------------------------------------
void HitEm::draw(const ds::GameTime& gameTime) {
	/*
	renderer->set2DCameraOn();
	renderer->applyMaterial(m_Mtrl);

	m_SpriteBatch->begin();
	
	m_SpriteBatch->draw(CENTER_X,CENTER_Y,ds::Rect(300,400,400,400),0.0f,1.8f,1.8f);
	m_SpriteBatch->draw(m_Bat.position.x,m_Bat.position.y,ds::Rect(32,0,45,41));
	for ( int i = 0;i < 4; ++i ) {
		Ball* ball = &m_Balls[i];
		m_SpriteBatch->draw(ball->position.x,ball->position.y,BALL_TEXTURES[ball->colorIndex]);
	}
	m_RedRing->draw(75.0f);
	m_GreenRing->draw(45.0f);
	m_SpriteBatch->end();	
	renderer->set2DCameraOff();
	*/
}

// -------------------------------------------------------
// Respawn ball
// -------------------------------------------------------
void HitEm::respawn(Ball* ball) {
	float angle = getRandomAngle(ball->colorIndex);
	float xp = CENTER_X + (RING_RADIUS - BALL_SIZE)* cos(DEGTORAD(angle));
	float yp = CENTER_Y + (RING_RADIUS - BALL_SIZE) * sin(DEGTORAD(angle));
	ball->entity.setPosition(ds::Vec2(xp,yp));
	//m_ObjectsQuadNode->update(ball->index,ball->position);
	angle += 180.0f;
	float vx = 300.0f * cos(DEGTORAD(angle));
	float vy = 300.0f * sin(DEGTORAD(angle));
	ball->velocity = ds::Vec2(vx,vy);
	ball->angle = angle;
	ball->mode = BM_GROWING;
	//ball->scale = 0.1f;
	ball->timer = 0.0f;
}
// -------------------------------------------------------
// Create ball
// -------------------------------------------------------
void HitEm::createBall(int colorIndex) {
	Ball* b = &m_Balls[colorIndex];	
	float angle = getRandomAngle(colorIndex);
	float xp = 512 + (RING_RADIUS - BALL_SIZE) * cos(DEGTORAD(angle));
	float yp = 384 + (RING_RADIUS - BALL_SIZE) * sin(DEGTORAD(angle));
	m_World.addSpriteEntity(0,0,&b->entity,xp,yp,BALL_TEXTURES[colorIndex]);
	angle += 180.0f;
	float vx = 300.0f * cos(DEGTORAD(angle));
	float vy = 300.0f * sin(DEGTORAD(angle));
	b->velocity = ds::Vec2(vx,vy);
	b->entity.setPosition(ds::Vec2(xp,yp));
	b->mode = BM_GROWING;
	b->timer = 0.0f;
	b->colorIndex = colorIndex;
	b->sticky = false;
	b->angle = angle;
	//b->scale = 1.0f;

}

// -------------------------------------------------------
// Check if position is inside goal
// -------------------------------------------------------
int HitEm::checkGoal(int mx,int my) {
	// upper goal
	if ( check2D(ds::Vec2(mx,my),450,35,120,20) ) {
		return 2;
	}
	// lower goal
	if ( check2D(ds::Vec2(mx,my),450,725,120,20) ) {
		return 0;
	}
	// left goal
	if ( check2D(ds::Vec2(mx,my),160,320,20,120) ) {
		return 1;
	}
	// right goal
	if ( check2D(ds::Vec2(mx,my),840,320,20,120) ) {
		return 3;
	}
	return -1;
}

// -------------------------------------------------------
// Check 2D
// -------------------------------------------------------
bool HitEm::check2D(ds::Vec2 ballPos,int x2,int y2,int w2,int h2) {
	int x1 = ballPos.x;
	int y1 = ballPos.y;
	int w1 = 32;
	int h1 = 32;	
	int left1 = x1;
	int left2 = x2;
	int right1 = left1 + w1;
	int right2 = left2 + w2;
	int top1 = y1;
	int top2 = y2;
	int bottom1 = top1 + h1;
	int bottom2 = top2 + h2;

	if (bottom1 < top2) return false;
	if (top1 > bottom2) return false;

	if (right1 < left2) return false;
	if (left1 > right2) return false;
	return true;
}

// -------------------------------------------------------
// Get random angle based on sector
// -------------------------------------------------------
float HitEm::getRandomAngle(int sector) {
	float start = 15.0f;
	float max = 75.0f;
	if ( sector == 1 ) {
		start = 105.0f;
	}
	else if ( sector == 2 ) {
		start = 195.0f;
	}
	else if ( sector == 3 ) {
		start = 285.0f;
	}
	return random(start,start+max);//start + rand()*max;
}

// -------------------------------------------------------
// On button down
// -------------------------------------------------------
void HitEm::OnButtonDown( int button,int x,int y ) {
	// if no ball is sticky
	if ( !m_Catcher.isActive()) {
		m_Catcher.setActive(true);
	}
	 if ( m_StickyBall == 0 ) {
		 for ( int i = 0; i < 4; ++i ) {
			 Ball* b = &m_Balls[i];
			 if ( b->mode != BM_GROWING ) {				 
				ds::Vec2 diff = b->entity.getPosition() - m_Bat.getPosition();
				if ( ds::vector::sqrLength(diff) <= (( BALL_RADIUS+PLAYER_RADIUS) * (BALL_RADIUS+PLAYER_RADIUS)) * 8.0f) {
					if ( m_StickyBall == 0 ) {
						b->angle = ds::math::getTargetAngle(m_Bat.getPosition(),b->entity.getPosition());
						b->sticky = true;
						m_StickyBall = b;
					}
				}
			 }
		 }
	 }
}

// -------------------------------------------------------
// On button up
// -------------------------------------------------------
void HitEm::OnButtonUp( int button,int x,int y ) {
	// if there actually is a sticky ball
	if ( m_StickyBall != 0 ) {
		m_StickyBall->sticky = false;
		m_StickyBall->velocity = ds::math::getRadialVelocity(RADTODEG(m_StickyBall->angle),300.0f);
		// release it and set new velocity
		m_StickyBall = 0;
		m_Catcher.setActive(false);
	}

	ds::Vec2 mp = getMousePos();
	mp.y = 768.0f - mp.y;
	ds::Vec2 v;
	v.x = mp.x - CENTER_X;
	v.y = mp.y - CENTER_Y;
	float da = ds::math::getAngle(ds::vector::normalize(v),ds::Vec2(1,0));
	LOG(logINFO) << "clicked da: " << RADTODEG(da);
}

void HitEm::OnChar( char ascii,unsigned int keyState ) {
	if ( ascii == 'a' ) {
		m_BorderExp.start(ds::Vec2(400,400));
	}
}
#pragma comment(lib, "Diesel2.lib")
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "dxerr.lib")
#pragma warning(disable : 4995)

#pragma once
#include "base\BaseApp.h"
#include "dxstdafx.h"
#include <renderer\render_types.h>
#include "EnergyRing.h"
#include <world\SpriteEntity.h>
#include "Goal.h"
#include <particles\ParticleEmitter.h>

class HitEm : public ds::BaseApp {

enum BallMode {
	BM_FLYING,
	BM_GROWING
};

struct Ball {
	ds::SpriteEntity entity;
	ds::Vec2 velocity;
	//ds::Vec2 position;
	int colorIndex;
	BallMode mode;
	float timer;
	bool sticky;
	float angle;
	//ds::Rect texRect;
	//float scale;
};

public:	
	HitEm();
	virtual ~HitEm();
	bool loadContent();
	const char* getTitle() {
		return "HitEm";
	}
	void update(const ds::GameTime& gameTime);
	void draw(const ds::GameTime& gameTime);
	void OnButtonDown( int button,int x,int y );
	void OnButtonUp( int button,int x,int y );
	void OnChar( char ascii,unsigned int keyState );
private:
	void createBall(int colorIndex);
	float getRandomAngle(int sector);
	bool isOutside(const ds::Vec2& pos);
	int checkGoal(int mx,int my);
	bool check2D(ds::Vec2 ballPos,int x2,int y2,int w2,int h2);
	void respawn(Ball* ball);
	ds::Vec2 reflect(const ds::Vec2& vel,const ds::Vec2& norm);
	float dot(const ds::Vec2& v1, const ds::Vec2& v2);	

	ds::SpriteEntity m_Background;
	ds::SpriteEntity m_Bat;
	ds::SpriteEntity m_Catcher;
	Goal m_Goal;
	ds::ParticlesystemEntity m_BorderExp;
	ds::ConeEmitter* m_BorderEmitter;

	int m_Mtrl;
	int m_BackgroundHandle;
	int m_RingHandle;
	int m_ObjectsHandle;

	ds::SpriteBatch* m_SpriteBatch;
	EnergyRing* m_RedRing;
	EnergyRing* m_GreenRing;
	//BounceParticlesystem* m_BounceParticles;
	Ball m_Balls[4];
	//Bat m_Bat;
	ds::BitmapFont* m_BitmapFont;
	//HUD* m_HUD;
	//ds::PostProcessor* m_PostProcessor;
	uint32 m_AddBlendState;
	uint32 m_DefaultBS;
	Ball* m_StickyBall;
	uint32 m_OuterRingIdx;
};